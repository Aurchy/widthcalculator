import sys
from bs4 import BeautifulSoup
from bs4.element import NavigableString, Tag

def calc_width(child_val, parent_val=None):
	if not parent_val:
		return 100
	return (child_val*100)/parent_val

def get_width(node):
	if not node or (not 'style' in node.attrs):
		return
	attrs = node['style'].split(';')
	for attr in attrs:
		attr = attr.strip()
		if attr.startswith('width:'):
			_, val = attr.split(':')
			val = int(val.replace('px', ' ').strip())
			return val
	return

def set_width(node, new_val):
	if not node or (not 'style' in node.attrs):
		return
	attrs = node['style'].split(';')
	rets = []
	for attr in attrs:
		if not attr:
			continue
		attr = attr.strip()
		if attr.startswith('width:'):
			rets.append('width: ' + str(new_val) + '%')
		else:
			rets.append(attr)
	node['style'] = '; '.join(rets)
	

def calc_and_update_width(node, parent_width):
	if not isinstance(node, Tag):
		return
	children = node.children
	width = get_width(node)
	for child in children:
		calc_and_update_width(child, width)
	width_in_prcntg = calc_width(width, parent_width)
	set_width(node, width_in_prcntg)

def write_new_line(line, current_indent, desired_indent):
	new_line = ""
	spaces_to_add = (current_indent * desired_indent) - current_indent
	if spaces_to_add > 0:
		for i in range(spaces_to_add):
			new_line += " "		
	new_line += str(line) + "\n"
	return new_line

def soup_prettify(soup, desired_indent):
	pretty_soup = str()
	previous_indent = 0
	for line in soup.prettify().split("\n"):
		current_indent = str(line).find("<")
		if current_indent == -1 or current_indent > previous_indent + 2:
			current_indent = previous_indent + 1
		previous_indent = current_indent
		pretty_soup += write_new_line(line, current_indent, desired_indent)
	return pretty_soup


file_name = "index.html"
soup = BeautifulSoup(open(file_name), 'html.parser')
root = soup.find()
calc_and_update_width(root, None)
soup = soup_prettify(soup, 2)
with open("output.html", "w") as file:
    file.write(str(soup))


